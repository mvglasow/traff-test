/*
 * Copyright (C) 2018–2020 Michael von Glasow
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.traffxml.trafftest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.traffxml.consumer.android.AndroidConsumer;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.Version;
import org.traffxml.traff.subscription.FilterItem;
import org.traffxml.traff.subscription.FilterList;
import org.traffxml.transport.android.AndroidTransport;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * The main activity.
 */
public class MainActivity extends AppCompatActivity {

	/** A broadcast receiver to process responses to ordered broadcasts */
	private BroadcastReceiver logReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if ((intent != null) && (intent.getAction().equals(Util.ACTION_INTERNAL_LOG)))
				messageAdapter.add(intent.getStringExtra(Util.EXTRA_DATA));
		}
	};

    /** The adapter through which the list view retrieves messages */
    private MessageAdapter messageAdapter;

    /** The UI element which displays the list of messages */
    private ListView messageView;

    /** The listener which fires when a list item is clicked */
    private OnItemClickListener messageClickListener;

    /** A broadcast receiver to process responses to ordered broadcasts */
    private BroadcastReceiver resultReceiver = new BroadcastReceiver() {

    	@Override
    	public void onReceive(Context context, Intent intent) {
    		if (intent != null) {
    			StringBuilder builder = new StringBuilder();
    			builder.append("Consumer: result for ").append(intent.getAction()).append("\n");
    			builder.append("Result: ").append(this.getResultCode());
    			if (intent.getAction().equals(AndroidTransport.ACTION_TRAFF_GET_CAPABILITIES)) {
    				/* we only ever send this request when we want to subscribe to the source */
    				Bundle extras = this.getResultExtras(true);
    				String packageName = extras.getString(AndroidTransport.EXTRA_PACKAGE);
    				if (extras.containsKey(AndroidTransport.EXTRA_PACKAGE))
    					builder.append("\n").append(extras.getString(AndroidTransport.EXTRA_PACKAGE));
    				if (extras.containsKey(AndroidTransport.EXTRA_CAPABILITIES))
    					builder.append("\n").append(extras.getString(AndroidTransport.EXTRA_CAPABILITIES));
    				messageAdapter.add(builder.toString());
    				/* subscribe */
    				List<FilterItem> filterItemList = new ArrayList<FilterItem>(1);
    				filterItemList.add(new FilterItem(new BoundingBox(-90, -180, 90, 180), null));
    				FilterList filterList = new FilterList(filterItemList);
    				if (packageName != null) {
    					Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_SUBSCRIBE);
    					PackageManager pm = MainActivity.this.getPackageManager();
    					List<ResolveInfo> receivers = pm.queryBroadcastReceivers(outIntent, 0);
    					if (receivers != null)
    						/*
    						 * this is actually a re-implementation of AndroidConsumer#sendTraffIntent(), with the
    						 * additional feature of logging a message for each broadcast sent
    						 */
    						for (ResolveInfo receiver : receivers) {
    							if (!packageName.equals(receiver.activityInfo.applicationInfo.packageName))
    								continue;
    							messageAdapter.add(String.format("Subscribing to %s#%s", receiver.activityInfo.applicationInfo.packageName,
    									receiver.activityInfo.name));
    							ComponentName cn = new ComponentName(receiver.activityInfo.applicationInfo.packageName,
    									receiver.activityInfo.name);
    							outIntent = new Intent(AndroidTransport.ACTION_TRAFF_SUBSCRIBE);
    							outIntent.setComponent(cn);
    							outIntent.putExtra(AndroidTransport.EXTRA_PACKAGE, MainActivity.this.getPackageName());
    							try {
    								outIntent.putExtra(AndroidTransport.EXTRA_FILTER_LIST, filterList.toXml());
    								sendOrderedBroadcast (outIntent, 
    										Manifest.permission.ACCESS_COARSE_LOCATION, // receiverPermission, 
    										resultReceiver, 
    										null, // scheduler, 
    										AndroidTransport.RESULT_INTERNAL_ERROR, // initialCode, 
    										null, // initialData, 
    										null); // initialExtras
    							} catch (Exception e) {
    								e.printStackTrace();
    							}
    						}
    				}
    			} else if (intent.getAction().equals(AndroidTransport.ACTION_TRAFF_SUBSCRIBE)) {
    				if (this.getResultCode() == AndroidTransport.RESULT_OK) {
    					Bundle extras = this.getResultExtras(true);
    					String packageName = extras.getString(AndroidTransport.EXTRA_PACKAGE);
    					String subscriptionId = extras.getString(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
    					String data = this.getResultData();
    					if (extras.containsKey(AndroidTransport.EXTRA_PACKAGE))
    						builder.append("\n").append(extras.getString(AndroidTransport.EXTRA_PACKAGE));
    					if (extras.containsKey(AndroidTransport.EXTRA_SUBSCRIPTION_ID))
    						builder.append("\n").append(extras.getString(AndroidTransport.EXTRA_SUBSCRIPTION_ID));
    					builder.append("\n").append(this.getResultData());
    					subscriptions.put(subscriptionId, packageName);
    					unsubscribe.setEnabled(true);
    					subscribe.setEnabled(false);
    					/* query the content provider */
    					if (data != null) {
    						Uri uri = Uri.parse(data);
    						Cursor cursor = getContentResolver().query(uri, new String[] {AndroidTransport.COLUMN_DATA}, null, null, null);
    						if (cursor == null)
    							builder.append("\nCursor is null");
    						else if (cursor.getCount() < 1)
    							builder.append("\nNo messages");
    						// FIXME debug only
    						else
    							builder.append("\n").append(cursor.getCount()).append(" messages");
    						if (cursor != null) {
    							/* FIXME re-enable when done debugging
        	        			while (cursor.moveToNext()) {
        	        				messageAdapter.add(cursor.getString(cursor.getColumnIndex(AndroidTransport.COLUMN_DATA)));
        	        				//itemsAdded++;
        	        			}
    							 */
    							cursor.close();
    						}
    					}
    				}
    				messageAdapter.add(builder.toString());
    			} else {
    				/* display only, no action */
    				builder.append("\n(no action)");
    				messageAdapter.add(builder.toString());
    			}
    		}
    	}

    };

    private MenuItem subscribe;

    private MenuItem unsubscribe;

    /** Active subscriptions (key is the subscription ID, value is the package ID) */
    private Map<String, String> subscriptions = new HashMap<String, String>();

    /** An intent filter for the intents processed by a TraFF consumer */
    private IntentFilter traffConsumerFilter = AndroidConsumer.createIntentFilter(Version.V0_8);

    /** An intent filter for the intents processed by a legacy TraFF consumer */
    private IntentFilter traffLegacyConsumerFilter = AndroidConsumer.createIntentFilter(Version.V0_7);

    /** A broadcast receiver to process feeds */
    private BroadcastReceiver traffReceiver = new BroadcastReceiver() {

    	@Override
    	public void onReceive(Context context, Intent intent) {
    		if (intent != null) {
    			if (intent.getAction().equals(AndroidTransport.ACTION_TRAFF_PUSH)) {
    				int itemsAdded = 1;
    				StringBuilder builder = new StringBuilder();
    				builder.append("Consumer: ").append(intent.getAction()).append("\n");
    				if (intent.hasExtra(AndroidTransport.EXTRA_PACKAGE))
    					builder.append(intent.getStringExtra(AndroidTransport.EXTRA_PACKAGE)).append("\n");
    				if (intent.hasExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID))
    					builder.append(intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID)).append("\n");
    				Uri uri = intent.getData();
    				if (uri != null) {
    					/* 0.8 feed */
    					builder.append(uri.toString());
    					String subscriptionId = intent.getStringExtra(AndroidTransport.EXTRA_SUBSCRIPTION_ID);
    					if (subscriptionId == null) {
    						builder.append("\nSubscription ID is null, ignoring");
    						messageAdapter.add(builder.toString());
    					} else if (!subscriptions.containsKey(subscriptionId)) {
    						builder.append("\nSubscription ID is unknown, unsubscribing");
    						messageAdapter.add(builder.toString());
    						/* unsubscribe */
    						Bundle extras = new Bundle();
    						extras.putString(AndroidTransport.EXTRA_SUBSCRIPTION_ID, subscriptionId);
    						AndroidConsumer.sendTraffIntent(context, AndroidTransport.ACTION_TRAFF_UNSUBSCRIBE, null, extras,
    								intent.getStringExtra(AndroidTransport.EXTRA_PACKAGE),
    								Manifest.permission.ACCESS_COARSE_LOCATION, resultReceiver);
    					} else {
    						/* valid subscription, display messages */
    						Cursor cursor = getContentResolver().query(uri, new String[] {AndroidTransport.COLUMN_DATA}, null, null, null);
    						if (cursor == null)
    							builder.append("\nCursor is null");
    						else if (cursor.getCount() < 1)
    							builder.append("\nNo messages");
    						messageAdapter.add(builder.toString());
    						if (cursor != null) {
    							while (cursor.moveToNext()) {
    								messageAdapter.add(cursor.getString(cursor.getColumnIndex(AndroidTransport.COLUMN_DATA)));
    								itemsAdded++;
    							}
    							cursor.close();
    						}
    					}
    				} else {
    					/* 0.7 feed */
    					String packageName = intent.getStringExtra(AndroidTransport.EXTRA_PACKAGE);
    					String feedString = intent.getStringExtra(AndroidTransport.EXTRA_FEED);
    					if (feedString == null) {
    						builder.append("(no data)");
    					} else {
    						Strategy strategy = new TreeStrategy("simpleXmlClass", "simpleXmlLength");
    						Serializer serializer = new Persister(strategy);
    						try {
    							TraffFeed feed = serializer.read(TraffFeed.class, feedString);
    							if ((packageName != null) && subscriptions.values().contains(packageName)) {
    								builder.append("(TraFF 0.7 feed of ").append(feed.getMessages().size()).append(" messages omitted because we have a subscription for the source)");
    							} else {
    								builder.append(feed.toXml());
    							}
    						} catch (Exception e) {
    							e.printStackTrace();
    							builder.append(e.getClass().getSimpleName());
    						}
    					}
    					messageAdapter.add(builder.toString());
    				}
    				if (messageView.getLastVisiblePosition() >= (messageAdapter.getCount() - 1 - itemsAdded))
    					messageView.smoothScrollToPosition(messageAdapter.getCount() - itemsAdded);
    			} else {
    				/* display only, no action */
    				messageAdapter.add(String.format("%s\n(no action)", intent.getAction()));
    			}
    		}
    	}
    };


    public MainActivity() {
    }

    /** Called with the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate our UI from its XML layout description.
        setContentView(R.layout.main_activity);

        messageView = (ListView) this.findViewById(R.id.messageView);
        messageAdapter = new MessageAdapter(MainActivity.this, R.layout.simple_list_item);
        messageView.setAdapter(messageAdapter);
        messageClickListener = new MessageClickListener();
        messageView.setOnItemClickListener(messageClickListener);

        // Make sure we have the necessary permissions
        ArrayList<String> perms = new ArrayList<String>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            perms.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!perms.isEmpty())
            ActivityCompat.requestPermissions(this, perms.toArray(new String[] {}), Util.PERM_REQUEST_LOCATION);
        // TODO
    }

    /**
     * Called when your activity's options menu needs to be created.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        subscribe = menu.findItem(R.id.action_subscribe);
        unsubscribe = menu.findItem(R.id.action_unsubscribe);
        return true;
    }

    /**
     * Called right before your activity's option menu is displayed.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        return true;
    }

    /**
     * Called when a menu item is selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_subscribe:
        	subscribeAll();
        	break;
        case R.id.action_unsubscribe:
        	unsubscribeAll();
        	subscribe.setEnabled(true);
        	unsubscribe.setEnabled(false);
        	break;
        case R.id.action_send:
            Util.sendFeed(this);
            break;
        case R.id.action_query:
            /* Broadcast a poll intent */
            Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_POLL);
            PackageManager pm = this.getPackageManager();
            List<ResolveInfo> receivers = pm.queryBroadcastReceivers(outIntent, 0);
            if (receivers != null)
                for (ResolveInfo receiver : receivers) {
                    messageAdapter.add(String.format("Polling %s#%s", receiver.activityInfo.applicationInfo.packageName,
                            receiver.activityInfo.name));
                    ComponentName cn = new ComponentName(receiver.activityInfo.applicationInfo.packageName,
                            receiver.activityInfo.name);
                    outIntent = new Intent(AndroidTransport.ACTION_TRAFF_POLL);
                    outIntent.setComponent(cn);
                    sendBroadcast(outIntent, Manifest.permission.ACCESS_COARSE_LOCATION);
                }
            break;
        case R.id.action_clear:
            messageAdapter.clear();
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if ((requestCode == Util.PERM_REQUEST_LOCATION) && (grantResults.length > 0))
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    if (permissions[i] == Manifest.permission.ACCESS_COARSE_LOCATION) {
                        String message = getString(R.string.status_perm_location);
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        Log.w(this.getClass().getSimpleName(),
                                "ACCESS_COARSE_LOCATION permission not granted. Poll requests may go unanswered.");
                    } else if (permissions[i] == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                        String message = getString(R.string.status_perm_storage);
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        Log.w(this.getClass().getSimpleName(),
                                "WRITE_EXTERNAL_STORAGE permission not granted. Cannot save feeds.");
                    }
            }
    }

    @Override
    protected void onPause() {
        this.unregisterReceiver(traffReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(logReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.registerReceiver(traffReceiver, traffConsumerFilter);
        this.registerReceiver(traffReceiver, traffLegacyConsumerFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(logReceiver,
        		new IntentFilter(Util.ACTION_INTERNAL_LOG));
    }

    private void subscribeAll() {
    	Intent outIntent = new Intent(AndroidTransport.ACTION_TRAFF_GET_CAPABILITIES);
    	PackageManager pm = this.getPackageManager();
    	List<ResolveInfo> receivers = pm.queryBroadcastReceivers(outIntent, 0);
    	if (receivers != null)
    		/*
    		 * this is actually a re-implementation of AndroidConsumer#sendTraffIntent(), with the
    		 * additional feature of logging a message for each broadcast sent
    		 */
    		for (ResolveInfo receiver : receivers) {
    			messageAdapter.add(String.format("Querying capabilities of %s#%s", receiver.activityInfo.applicationInfo.packageName,
    					receiver.activityInfo.name));
    			ComponentName cn = new ComponentName(receiver.activityInfo.applicationInfo.packageName,
    					receiver.activityInfo.name);
    			outIntent = new Intent(AndroidTransport.ACTION_TRAFF_GET_CAPABILITIES);
    			outIntent.setComponent(cn);
    			sendOrderedBroadcast (outIntent, 
    					null, // receiverPermission, 
    					resultReceiver, 
    					null, // scheduler, 
    					AndroidTransport.RESULT_INTERNAL_ERROR, // initialCode, 
    					null, // initialData, 
    					null);
    		}
    }

    private void unsubscribeAll() {
    	for (Map.Entry<String, String> entry : subscriptions.entrySet()) {
    		Bundle extras = new Bundle();
    		extras.putString(AndroidTransport.EXTRA_SUBSCRIPTION_ID, entry.getKey());
    		AndroidConsumer.sendTraffIntent(this, AndroidTransport.ACTION_TRAFF_UNSUBSCRIBE, null, extras, entry.getValue(),
    				Manifest.permission.ACCESS_COARSE_LOCATION, resultReceiver);
    	}
    	subscriptions.clear();
    }

    /**
     * @brief An adapter to display messages in a ListView. 
     */
    private class MessageAdapter extends ArrayAdapter<String> {
        private MessageAdapter(Context context, int resource) {
            super(context, resource);
        }
    }

    /**
     * @brief An OnClickListener which saves messages to storage.
     */
    private class MessageClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                String feed = messageAdapter.getItem(position);
                File dumpDir = MainActivity.this.getExternalFilesDir(null);
                DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ROOT);
                fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                String fileName = String.format("traff-%s.xml", fmt.format(new Date(System.currentTimeMillis())));
                File dumpFile = new File (dumpDir, fileName);
                PrintStream s;
                try {
                    s = new PrintStream(dumpFile);
                    s.append(feed);
                    s.flush();
                    s.close();
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(dumpFile);
                    mediaScanIntent.setData(contentUri);
                    MainActivity.this.sendBroadcast(mediaScanIntent);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                String message = getString(R.string.status_saved);
                Toast.makeText(MainActivity.this, String.format(message, fileName), Toast.LENGTH_SHORT).show();
            } else {
                String message = getString(R.string.status_perm_storage);
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                Log.w(this.getClass().getSimpleName(),
                        "WRITE_EXTERNAL_STORAGE permission not granted. Cannot save feeds.");
            }
        }
    }
}

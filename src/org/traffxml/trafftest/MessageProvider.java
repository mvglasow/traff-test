package org.traffxml.trafftest;

import org.traffxml.transport.android.AndroidTransport;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * A content provider through which other applications can retrieve TraFF messages.
 */
public class MessageProvider extends ContentProvider {
	private static final String TAG = "MessageProvider";

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		return AndroidTransport.MIME_TYPE_TRAFF;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public boolean onCreate() {
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		/* the only allowed column name is AndroidTransport.COLUMN_DATA ("data") */
		for (String column : projection) {
			if (!column.equalsIgnoreCase(AndroidTransport.COLUMN_DATA))
				throw new IllegalArgumentException(String.format("'%s' is not a legal column name", column));
		}
		/* selection is not supported */
		if ((selection != null) && !selection.isEmpty())
			throw new IllegalArgumentException("Specifying a selection is not supported");
		/* sort order is not supported */
		if ((sortOrder != null) && !sortOrder.isEmpty())
			throw new IllegalArgumentException("Specifying a sort order is not supported");
		String id = uri.getPath();
		if (id == null)
			throw new IllegalArgumentException("Subscription ID must not be null");
		
		Intent logIntent = new Intent(Util.ACTION_INTERNAL_LOG);
		StringBuilder builder = new StringBuilder();
		builder.append("Provider: ").append(uri.toString());
		
		if (id.startsWith("/"))
			id = id.substring(1);
		if (id.equals(Util.SUBSCRIPTION_ID_DUMMY)) {
			builder.append("\nReturning 0 messages");
			Log.d(TAG, builder.toString());
			logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
			LocalBroadcastManager.getInstance(this.getContext()).sendBroadcast(logIntent);
			return new MatrixCursor(new String[]{AndroidTransport.COLUMN_DATA});
		} else {
			builder.append("\nInvalid subscription, returning null cursor");
			Log.d(TAG, builder.toString());
			logIntent.putExtra(Util.EXTRA_DATA, builder.toString());
			LocalBroadcastManager.getInstance(this.getContext()).sendBroadcast(logIntent);
			return null;
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}
}
